using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region Singleton Manager
    private static DataManager _instance;

    public static DataManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("DataManager is NULL");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }
    #endregion

    private float _currentCoin;
    public float CurrentCoin
    {
        get
        {
            return _currentCoin;
        }
        set
        {
            _currentCoin = value;
            UIManager.Instance.CoinUpdate();
        }
    }

    // Upgrade level
    public int[] upgradeLevel;

    private int _currentStage;

    public int CurrentStage
    {
        get
        {
            return _currentStage;
        }
        set
        {
            _currentStage = value;
            UIManager.Instance.StageUpdate();
        }
    }
    private void Start()
    {


    }
}
