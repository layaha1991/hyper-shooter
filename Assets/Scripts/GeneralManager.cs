using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralManager : MonoBehaviour
{
    #region Singleton Manager
    private static GeneralManager _instance;

    public static GeneralManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GeneralManager is NULL");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
        for (int i = 0; i < manager.Length; i++)
        {
            manager[i].SetActive(true);
        }
    }
    #endregion

    public GameObject[] manager;


}
