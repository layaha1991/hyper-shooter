using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class player : MonoBehaviour, IDragHandler
{ 

    [SerializeField] private Camera mainCamera;
    [SerializeField] private Transform spawnPos;
private IEnumerator shootCoroutine;

    

    private void Start()
    {
        
    }
    private void OnEnable()
    {
 shootCoroutine = Shooting();
        StartCoroutine(shootCoroutine);

        this.transform.position = GameplayManager.Instance.playerSpawnPos.position;
    }

    private void OnDisable()
    {
              StopCoroutine(shootCoroutine);
    }

    private void Update()
    {
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    IEnumerator Shooting()
    {
        
        while (true)
        {
            GameObject bullet = GameplayManager.Instance.GetBullet("player",GameplayManager.Instance.currentBulletId);
            bullet.transform.position = spawnPos.position;
            bullet.transform.rotation = Quaternion.identity;
            bullet.SetActive(true);

            yield return new WaitForSeconds(GameplayManager.Instance.attackSpeed);
        }
    }

    public void OnTriggerEnter2D(Collider2D col)
    {

        if (col.tag == "enemyBullet")
        {
            GameplayManager.Instance.hp -= col.GetComponent<enemyBullet_general>().parentDamage;

            if ( GameplayManager.Instance.hp<= 0)
            {
                this.gameObject.SetActive(false);
                GameplayManager.Instance.GameEnd();
            }

            // Target Hit!
            col.gameObject.SetActive(false);

        }
    }



}
