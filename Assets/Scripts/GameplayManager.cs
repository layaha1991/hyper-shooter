using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

[System.Serializable]
public class Enemy
{
    public float hp;
    public float shootInterval;
    public float damage;
    public int bulletId; // choose bullet type
    public float giveCoin;
    public Transform bulletSpawnPos;
}
[System.Serializable] public class Bullet
{
    
    public float speed;
    public float destoryTime;

}

public class GameplayManager : MonoBehaviour
{
    #region Singleton Manager
    private static GameplayManager _instance;

    public static GameplayManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameplayManager is NULL");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
        
        for (int j=0; j< playerBulletPool.Length;j++)
        {

                playerBulletPool[j] = new List<GameObject>(poolSize);
        }
       
        for (int j=0; j<enemyBulletPool.Length;j++)
        {

                enemyBulletPool[j] = new List<GameObject>(poolSize);

        }
   
    

        for(int j =0; j<playerBulletPrefab.Length;j++)
        {
            for (int i = 0; i < poolSize; i++)
            {
                GameObject bullet = Instantiate(playerBulletPrefab[j], bulletParent.transform);
                bullet.SetActive(false);

                playerBulletPool[j].Add(bullet);
               
            }
        }

        for (int j = 0; j < enemyBulletPrefab.Length; j++)
        {
            for (int i = 0; i < poolSize; i++)
            {
                GameObject bullet = Instantiate(enemyBulletPrefab[j], bulletParent.transform);
                bullet.SetActive(false);
                enemyBulletPool[j].Add(bullet);
            }
        }


    }
    #endregion

    // Plane Stat
    
    public float attack;
    public float shield;
    public float hp;
    public int currentBulletId;
    public float attackSpeed;


    public GameObject player;
    public Transform playerSpawnPos;


    // Bullet Pool
    public GameObject[] playerBulletPrefab;
    public GameObject[] enemyBulletPrefab;
    public int poolSize = 100;

    [SerializeField] private List<GameObject>[] playerBulletPool = new List<GameObject>[99];
    [SerializeField] private List<GameObject>[] enemyBulletPool = new List<GameObject>[99];
    [SerializeField] private GameObject bulletParent;


    public GameObject GetBullet(string type, int bulletId)
    {

        if(type =="enemy")
        {
            
            for (int i=0; i<enemyBulletPool[bulletId].Count;i++)
            {
                if(enemyBulletPool[bulletId][i].activeInHierarchy == false)
                {
                    return enemyBulletPool[bulletId][i];
                }
                
            }
   
            GameObject bullet = Instantiate(enemyBulletPrefab[bulletId], bulletParent.transform);
            bullet.SetActive(false);
            enemyBulletPool[bulletId].Add(bullet);
            return bullet;

        }
        else
        {
          
            for (int i = 0; i < playerBulletPool[bulletId].Count; i++)
            {
           
                if (playerBulletPool[bulletId][i].activeInHierarchy == false)
                {
   
                    return playerBulletPool[bulletId][i];
                   
                }

            }

            GameObject bullet = Instantiate(playerBulletPrefab[bulletId], bulletParent.transform);
            bullet.SetActive(false);
            playerBulletPool[bulletId].Add(bullet);
            return bullet;
        }
        
    }


    // Game Setup
    public bool isStageEnd;
    public bool isAllEnemyDead;
    [SerializeField] private Button triggerButton;
    private void Start()
    {

    }

     public void GameStart()
    {
        triggerButton.gameObject.SetActive(false);
        UpgradeManager.Instance.ShowHideUpgradePanel();
        player.SetActive(true);
       
        stageManager.Instance.StartStage();

    }

    public void GameEnd()
    {
        Debug.Log("GameEnd");
        UpgradeManager.Instance.ShowHideUpgradePanel();
        triggerButton.gameObject.SetActive(true);
        player.SetActive(false);
        DataManager.Instance.CurrentStage++;
       
    }

}
