using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class triggerButton : MonoBehaviour
{
    private void Start()
    {
      Button button = GetComponent<Button>();
        button.onClick.AddListener(triggerButtonOnClickEvent);  
    }

    private void triggerButtonOnClickEvent()
    {
        GameplayManager.Instance.GameStart();
        
    }
}
