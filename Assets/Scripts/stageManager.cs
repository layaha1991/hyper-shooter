using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class stageManager : MonoBehaviour
{

    #region Singleton Manager
    private static stageManager _instance;

    public static stageManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("stageManager is NULL");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }
    #endregion

    public GameObject[] spawner;

    public GameObject[] target;

    public GameObject[] enemyPrefab;
    [SerializeField]
    private GameObject enemyPool;

    private IEnumerator startStageCoroutine;

    public StageSetup stageSetup; // reference to the StageSetup component that contains the stage data

    private int currentStageIndex = 0; // current stage index

    private void Start()
    {
        
    }

public void StartStage()
    {
        startStageCoroutine = StartStageContent();
        StartCoroutine(startStageCoroutine);
    }

    private IEnumerator StartStageContent()
    {
        GameplayManager.Instance.isStageEnd = false;
        while (true)
        {
            // Reset Param
            


            if (DataManager.Instance.CurrentStage >= stageSetup.stages.Count)
            {
                Debug.Log("All stages completed!");
                break;
            }

            // get the current stage data
            StageSetup.StageData stageData = stageSetup.stages[DataManager.Instance.CurrentStage];


            // loop through each stage action and perform it
            foreach (StageSetup.StageAction action in stageData.actions)
            {

                switch (action.type)
                {
                    case StageSetup.ActionType.Wait:
                        yield return new WaitForSeconds(action.waitTime);

                        break;
                    case StageSetup.ActionType.Spawn:
                        SpawnEnemy(action.enemyType, action.spawnPosition, action.targetPosition, action.moveTime);

                        break;
                    case StageSetup.ActionType.End:

                        StopCoroutine(startStageCoroutine);
                        GameplayManager.Instance.isStageEnd = true;

                        break;
                }
            }
        }
    }



    private void SpawnEnemy(GameObject enemyPrefab, Transform spawnPosition, Transform targetPosition, float moveTime)
    {
        // spawn the enemy prefab at the spawn position
        GameObject enemy = Instantiate(enemyPrefab, spawnPosition.position, spawnPosition.rotation, enemyPool.transform);
        enemy.GetComponent<enemyGeneral>().spawnPos = spawnPosition;
        enemy.GetComponent<enemyGeneral>().targetPos = targetPosition;
        enemy.GetComponent<enemyGeneral>().moveTime = moveTime;
        // set the enemy's target to the target position


    }
    public void CheckLastEnemy()
    {
        Debug.Log("CheckEndStage - childCount: " + enemyPool.transform.childCount);
        if(enemyPool.transform.childCount <=1 && GameplayManager.Instance.isStageEnd == true) // Including the last enemy
        {
            Debug.Log("All Dead");
            GameplayManager.Instance.GameEnd();
        }
    }

}
