using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Singleton Manager
    private static UIManager _instance;

    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("UIManager is NULL");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }
    #endregion

    [SerializeField] private Text coin;
    [SerializeField] private Text stage;

    public void Start()
    {
        CoinUpdate();
        StageUpdate();
    }

    public void StageUpdate()
    {
        stage.text = "Stage: " + (DataManager.Instance.CurrentStage+1).ToString();
    }
    public void CoinUpdate()
    {
        coin.text = DataManager.Instance.CurrentCoin.ToString();
    }

}
