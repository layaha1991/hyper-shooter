using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class upgradeButton : MonoBehaviour
{
    
    [SerializeField] private Text title;
    [SerializeField] private Image icon;
    [SerializeField] private Text cost;
    [SerializeField] private Upgrade upgrade;

    private void Start()
    {
        title.text = upgrade.upgradeTitle;
        this.GetComponent<Button>().onClick.AddListener(() => UpgradeButtonOnClickEvent());
        UpdateCost();
    }

    private void UpdateCost()
    {
        cost.text = "$ " + upgrade.Cost.ToString();
        
    }

    
    private void UpgradeButtonOnClickEvent()
    {
        if(DataManager.Instance.CurrentCoin >= upgrade.Cost)
        {
            DataManager.Instance.CurrentCoin -= upgrade.Cost;
            DataManager.Instance.upgradeLevel[upgrade.upgradeId]++;
            UpdateCost();
        }else
        {
            Debug.Log("Not Enough Coin");
        }
        
    }
}
