using UnityEngine;
using UnityEditor;
using static UnityEngine.GraphicsBuffer;

[CustomEditor(typeof(StageSetup))]
public class StageSetupEditor : Editor
{
    private Vector2 scrollPosition = Vector2.zero;

    public override void OnInspectorGUI()
    {
        // Get a reference to the StageSetup script
        StageSetup stageSetup = (StageSetup)target;

        EditorGUILayout.Space();

        // Display buttons for adding and duplicating stages
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Stage"))
        {
            stageSetup.stages.Add(new StageSetup.StageData()
            {
                actions = new System.Collections.Generic.List<StageSetup.StageAction>()
                {
                    new StageSetup.StageAction()
                    {
                        type = StageSetup.ActionType.Wait,
                        waitTime = 0f
                    }
                }
            });
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        // Display each stage in a scrollable area
        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
        for (int i = 0; i < stageSetup.stages.Count; i++)
        {
            StageSetup.StageData stageData = stageSetup.stages[i];

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            EditorGUI.indentLevel++;

            // Display the stage number and the foldout control for the actions
            stageData.foldout = EditorGUILayout.Foldout(stageData.foldout, "Stage " + (i + 1), true, EditorStyles.boldLabel);

            if (stageData.foldout)
            {
                EditorGUILayout.Space();

                // Display each action in a foldout group
                for (int j = 0; j < stageData.actions.Count; j++)
                {
                    StageSetup.StageAction stageAction = stageData.actions[j];

                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);

                    EditorGUI.indentLevel++;

                    // Display the type of action and wait time fields for the action
                    stageAction.type = (StageSetup.ActionType)EditorGUILayout.EnumPopup("Action Type", stageAction.type);

                    if (stageAction.type == StageSetup.ActionType.Wait) // only display wait time for Wait action
                    {
                        stageAction.waitTime = EditorGUILayout.FloatField("Wait Time", stageAction.waitTime);
                    }

                    // Display the enemy type, spawn position, and target position fields for the Spawn action
                    if (stageAction.type == StageSetup.ActionType.Spawn)
                    {
                        stageAction.enemyType = (GameObject)EditorGUILayout.ObjectField("Enemy Type", stageAction.enemyType, typeof(GameObject), true);
                        stageAction.spawnPosition = (Transform)EditorGUILayout.ObjectField("Spawn Position", stageAction.spawnPosition, typeof(Transform), true);
                        stageAction.targetPosition = (Transform)EditorGUILayout.ObjectField("Target Position", stageAction.targetPosition, typeof(Transform), true);
                        stageAction.moveTime = EditorGUILayout.FloatField("Move Time", stageAction.moveTime);
                    }

                    EditorGUI.indentLevel--;

                    EditorGUILayout.BeginHorizontal();

                    // Display a button for removing the action
                    if (GUILayout.Button("-", GUILayout.Width(20)))
                    {
                        stageData.actions.RemoveAt(j);
                        break;
                    }

                    GUILayout.FlexibleSpace();

                    // Display a button for duplicating the action
                    if (GUILayout.Button("+", GUILayout.Width(20)))
                    {
                        StageSetup.StageAction newStageAction = new StageSetup.StageAction();
                        newStageAction.type = stageAction.type;
                        newStageAction.waitTime = stageAction.waitTime;
                        newStageAction.enemyType = stageAction.enemyType;
                        newStageAction.spawnPosition = stageAction.spawnPosition;
                        newStageAction.targetPosition = stageAction.targetPosition;
                        newStageAction.moveTime = stageAction.moveTime;

                        stageData.actions.Insert(j + 1, newStageAction);
                        break;
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.EndVertical();
                }

                EditorGUILayout.Space();

                // Display a button for adding a new action
                if (GUILayout.Button("Add Action"))
                {
                    stageData.actions.Add(new StageSetup.StageAction());
                }
            }

            EditorGUI.indentLevel--;

            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
        }
        EditorGUILayout.EndScrollView();

        // Remove any empty stages
        for (int i = stageSetup.stages.Count - 1; i >= 0; i--)
        {
            if (stageSetup.stages[i].actions.Count == 0)
            {
                stageSetup.stages.RemoveAt(i);
            }
        }
    }
}