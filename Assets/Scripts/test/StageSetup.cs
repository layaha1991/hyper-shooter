using UnityEngine;
using System.Collections.Generic;

public class StageSetup : MonoBehaviour
{
    // Data structure for storing stage information
    [System.Serializable]
    public class StageData
    {
        public List<StageAction> actions = new List<StageAction>();
        public bool foldout = true; // add a foldout field to track whether the stage foldout control is open or closed
    }

    // Data structure for storing stage actions
    [System.Serializable]
    public class StageAction
    {
        public ActionType type;
        public float waitTime;
        public GameObject enemyType;
        public Transform spawnPosition;
        public Transform targetPosition;
        public float moveTime;
    }

    // Enum for different types of stage actions
    public enum ActionType
    {
        Wait,
        Spawn,
        End
    }

    // List of stages
    public List<StageData> stages = new List<StageData>();
}