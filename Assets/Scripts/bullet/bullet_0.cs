using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_0 : MonoBehaviour
{
    [SerializeField]
    private Bullet bullet;
    void Start()
    {
        
    }
    public float speed;
    public float destoryTime;
    private float counter;
    private void Update()
    {
        
        transform.Translate(Vector3.up * speed * Time.deltaTime, Space.Self);
        counter += Time.deltaTime;
        if(counter>=destoryTime)
        {
            counter = 0;
            this.gameObject.SetActive(false);
        }

    }
    private void OnDisable()
    {
        counter = 0;
    }
}
