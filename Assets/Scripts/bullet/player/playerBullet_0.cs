using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerBullet_0 : MonoBehaviour
{
    [SerializeField]
    private Bullet bullet;
    private float counter;
    private Vector3 direction;

    void Start()
    {
 // Player Bullet can only be straight
direction = Vector3.up;
       
        
    }
    private void Update()
    {
        
        transform.Translate(direction* bullet.speed * Time.deltaTime, Space.Self);
        counter += Time.deltaTime;
        if(counter>=bullet.destoryTime)
        {
            counter = 0;
            this.gameObject.SetActive(false);
        }

    }
   
       

    
}
