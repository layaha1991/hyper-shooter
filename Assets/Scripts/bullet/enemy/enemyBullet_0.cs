using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBullet_0 : MonoBehaviour
{
    [SerializeField] private Bullet bullet;
    private float counter;
    private Vector3 direction;


    
    private void OnEnable()
    {
        if(GameplayManager.Instance.player !=null)
        {
           Transform a = GameplayManager.Instance.player.transform;
            direction = (a.position - transform.position).normalized;
        }
           
    }
    private void Update()
    {
        
        transform.Translate(direction* bullet.speed * Time.deltaTime, Space.Self);
        counter += Time.deltaTime;
        if(counter>=bullet.destoryTime)
        {
            counter = 0;
            this.gameObject.SetActive(false);
        }

    }

    private void OnDisable()
    {
        counter = 0;
    }
}
