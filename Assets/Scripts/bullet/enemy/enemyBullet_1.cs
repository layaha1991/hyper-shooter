using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBullet_1 : MonoBehaviour
{
    [SerializeField] private Bullet bullet;
    private float counter;
    private Vector3 direction;
    
    private void OnEnable()
    {

            direction = Vector3.down;
    }
    private void Update()
    {
        
        transform.Translate(direction* bullet.speed * Time.deltaTime, Space.Self);
        counter += Time.deltaTime;
        if(counter>=bullet.destoryTime)
        {
            counter = 0;
            this.gameObject.SetActive(false);
        }

    }

    private void OnDisable()
    {
        counter = 0;
    }
}
