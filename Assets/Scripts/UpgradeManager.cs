using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;



[System.Serializable]
public class Upgrade
{

    public string upgradeTitle;
    public int upgradeId;
    public int baseCost;
    public float costMultiplier;
    public int functionId;
    public string iconPath;
    public float Cost
    {
        get
        {
            UIManager.Instance.CoinUpdate();
            return Mathf.RoundToInt(baseCost * Mathf.Pow(costMultiplier, DataManager.Instance.upgradeLevel[upgradeId]));
        }

    }
}
public class UpgradeManager : MonoBehaviour
{
    #region Singleton Manager
    private static UpgradeManager _instance;

    public static UpgradeManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("UpgradeManager is NULL");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }
    #endregion

    // for animation
    public GameObject upgradePanel;
    private IEnumerator HideUpgradePanelCoroutine;
    private IEnumerator ShowUpgradePanelCoroutine;
    [SerializeField] private Transform upgradePanelHidePos;
    [SerializeField] private Transform upgradePanelShowPos; 

    public void ShowHideUpgradePanel()
    {
        if (upgradePanel.activeSelf)
        {
            HideUpgradePanelCoroutine = HideUpgradePanel();
            StartCoroutine(HideUpgradePanelCoroutine);
        }
        else
        {
            ShowUpgradePanelCoroutine = ShowUpgradePanel();
            StartCoroutine(ShowUpgradePanelCoroutine);
        }
    }   
    private IEnumerator HideUpgradePanel()
    {
  
        while(true)
        {
            Vector3 dir = upgradePanelHidePos.position - upgradePanel.transform.position;
            upgradePanel.transform.Translate(dir * Time.deltaTime * 2); // Speed =2
            if(Vector3.Distance(upgradePanelHidePos.position,upgradePanel.transform.position) <=0.01f)
            {
                upgradePanel.SetActive(false);
                break;
            }
   
            yield return null;
        }
    }

    private IEnumerator ShowUpgradePanel()
    {
        upgradePanel.SetActive(true);
        while (true)
        {
            Vector3 dir = upgradePanelShowPos.position - upgradePanel.transform.position;
            upgradePanel.transform.Translate(dir * Time.deltaTime * 2); // Speed =2
            if (Vector3.Distance(upgradePanelHidePos.position, upgradePanel.transform.position) <= 0.01f)
            {
                
                break;
            }

            yield return null;
        }
    }

}
