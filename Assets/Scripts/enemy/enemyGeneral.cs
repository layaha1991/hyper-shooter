using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyGeneral : MonoBehaviour
{

    [SerializeField]
    private Enemy enemy;
    
    private IEnumerator shootCoroutine;

    private IEnumerator moveCoroutine;

    public Transform spawnPos;

    public Transform targetPos; // move target

    public float moveTime;


    private void OnEnable()
    {
        
        moveCoroutine = MoveCoroutine();
        shootCoroutine = Shoot();
        StartCoroutine(moveCoroutine);
        StartCoroutine(shootCoroutine);
    }
    private void OnDisable()
    {
        StopCoroutine(shootCoroutine);
    }

    IEnumerator Shoot()
    {
        while(true)
        {
            GameObject bullet = GameplayManager.Instance.GetBullet("enemy", enemy.bulletId);
            bullet.transform.position = enemy.bulletSpawnPos.position;
            bullet.transform.rotation = Quaternion.identity;
            bullet.GetComponent<enemyBullet_general>().parent = this.gameObject;
            bullet.GetComponent<enemyBullet_general>().parentDamage = enemy.damage;
            bullet.SetActive(true);
            yield return new WaitForSeconds(enemy.shootInterval);
        }
    }

    IEnumerator MoveCoroutine()
    {
        yield return new WaitForSeconds(0.2f); // For init Purpose or position is not set
        float elapsedTime = 0f;
        float duration = moveTime;
        
        while (true)
        {
            
            elapsedTime += Time.deltaTime;
            var t = elapsedTime / moveTime;
            transform.position = Vector3.Slerp(spawnPos.position, targetPos.position, t);
            if(elapsedTime >=duration)
            {
                break;
            }

            yield return null;
        }
        StopCoroutine(moveCoroutine);
       
    }


    public void OnTriggerEnter2D(Collider2D col)
    {
     
        if (col.tag == "playerBullet")
        {
            // Target Hit!
            col.gameObject.SetActive(false);
            enemy.hp -= GameplayManager.Instance.attack;

            if(enemy.hp<=0)
            {
                
                DataManager.Instance.CurrentCoin += enemy.giveCoin * (DataManager.Instance.CurrentStage+1);
                stageManager.Instance.CheckLastEnemy();
                Destroy(this.gameObject);
            }
            
            

        }
    }
}
